package com.mdp.mq.queue;

import org.springframework.stereotype.Service;

@Service
public class MsgList extends MessageListener<String> {

	@Override
	public Object getQueueKey() {
		
		return "test01";
	}

	@Override
	public void handleMessage(String message) {
		System.out.println("收到的消息为");
		System.out.println(message);
		
	}

}
