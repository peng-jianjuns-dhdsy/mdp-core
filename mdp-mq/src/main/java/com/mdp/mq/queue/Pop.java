package com.mdp.mq.queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class Pop {
	
	@Autowired
	RedisTemplate redisTemplate;
	
	/*
	 * 毫秒
	 */
	public Long timeOut() {
		return (long) 1000;
	}
	
	public <T> T rightPop(Object key){
		return (T) redisTemplate.opsForList().rightPop(key, timeOut(), TimeUnit.MILLISECONDS); 
	}
	
	public <T> T  rightPop(Object key,Long millseconds){
		return (T) redisTemplate.opsForList().rightPop(key, millseconds, TimeUnit.MILLISECONDS);
	}
	
	public Long  size(Object key){
		return redisTemplate.opsForList().size(key);
	}
	
	public <T> List<T> range(Object key,long start,long end){
		return  redisTemplate.opsForList().range(key, start, end);
	}
}
