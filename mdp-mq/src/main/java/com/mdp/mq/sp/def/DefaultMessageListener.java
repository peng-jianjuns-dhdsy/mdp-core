package com.mdp.mq.sp.def;

import com.mdp.mq.sp.SubscriberProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Service;

@Service
public class DefaultMessageListener implements MessageListener {

    @Autowired
    SubscriberProvider subscriberProvider;


    RedisSerializer<String> stringSerializer=RedisSerializer.string();

    @Override
    public void onMessage(Message message, byte[] bytes) {
        String channalName=stringSerializer.deserialize(bytes);
        subscriberProvider.receiveMessage(channalName,message);
    }
}
