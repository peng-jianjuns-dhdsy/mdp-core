package com.mdp.mq.sp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChannelConfigProvider {
	
	@Autowired(required=false)
	List<ChannelConfig> configs;
	
	public void container(RedisMessageListenerContainer container) { 
		if(configs==null) {
			return;
		}
	   for (ChannelConfig channelConfig : configs) {
		   channelConfig.container(container);
	   }
	}
	
	
}
