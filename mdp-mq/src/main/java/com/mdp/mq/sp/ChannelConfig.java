package com.mdp.mq.sp;

import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.Topic;

public interface ChannelConfig {
	
	RedisMessageListenerContainer container(RedisMessageListenerContainer container);
	
	void setMessageListener(MessageListener messageListener,Topic topic);
	
	void removeMessageListener(MessageListener messageListener,Topic topic);
	
	void removeMessageListener(MessageListener messageListener);

	void removeMessageListener(String listenerName,Topic topic);

	void addMessageListener(String listenerName,Topic topic);
	
}
