package com.mdp.micro.client;

import com.mdp.core.utils.LogUtils;

public class UrlWrapper {

    public static String urlWrapper(String url) {
        String gloNo= LogUtils.getGloNo(true);
        if(url.contains("?")) {
            url=url+"&gloNo="+gloNo;
        }else {
            url=url+"?gloNo="+gloNo;
        }
        return url;
    }
    public static String withApiGate(String url,String apiGate) {
        if(url.startsWith("www") || url.startsWith("http")){
            return url;
        }else{
            url=apiGate+url;
        }
        return url;
    }
}
