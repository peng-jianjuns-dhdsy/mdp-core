package com.mdp.dev.ctrl;

import com.mdp.core.entity.Result;
import com.mdp.core.err.BizException;
import com.mdp.core.utils.ObjectTools;
import com.mdp.dev.dao.CodeGenDao;
import com.mdp.dev.dao.GenDataDao;
import com.mdp.dev.entity.CodeGenVo;
import com.mdp.dev.main.CodeBase;
import com.mdp.dev.service.CodeGenService;
import com.mdp.dev.service.DevFileService;
import com.mdp.dev.service.TempleteService;
import com.mdp.dev.utils.ArcTools;
import com.mdp.dev.utils.ZipUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author maimeng-mdp code-gen
 * @since 2023-9-22
 */
@RestController
@RequestMapping(value="/**/dev/code")
@Api(tags={"代码生成器-在线生成器"})
public class CodeGenController {
	
	static Logger logger =LoggerFactory.getLogger(CodeGenController.class);


	/**生成代码*/
	@Autowired
	CodeGenDao codeGenDao;

	/**生成数据*/
	@Autowired
	GenDataDao genDataDao;


	@Autowired
	TempleteService templateService;

	@Autowired
	ResourceLoader resourceLoader;

	@Autowired
	DevFileService devFileService;


	@ApiOperation( value = "生成代码",notes=" ")
 	@ApiResponses({
		@ApiResponse(code = 200,response= Result.class,message = "{tips:{isOk:true/false,msg:'成功/失败原因',tipscode:'错误码'},total:总记录数,data:[数据对象1,数据对象2,...]}")
	})
	@RequestMapping(value="/gen",method=RequestMethod.GET)
	public ResponseEntity gen(CodeGenVo params,HttpServletRequest request, HttpServletResponse response) throws IOException {
		if(ObjectTools.isEmpty(params.getJavaPackage())){
			throw new BizException("javaPackage-required","javaPackage不能为空，该参数决定代码文件最终存放位置");
		}
		if(ObjectTools.isEmpty(params.getTableNames())){
			throw new BizException("tableNames-required","表名不能为空，该参数决定代码文件最终存放位置");
		}

		params.setForceOveride(true);
		String baseId=devFileService.createKey();
		String basePath=ArcTools.pathJoin(true,devFileService.getArcUploadRootPath(),baseId);
		params.setServiceProjectPath(ArcTools.pathJoin(true,basePath,"server_prj"));
		params.setViewProjectPath(ArcTools.pathJoin(true,basePath,"ui_prj"));
		CodeGenService codeGenService=new CodeGenService();
		codeGenService.setCodeGenDao(codeGenDao);
		codeGenService.setGenDataDao(genDataDao);
		codeGenService.setTemplateService(templateService);
		CodeBase codeBase=new CodeBase();
		codeBase.setCodeGenService(codeGenService);
		String[] tableNames=params.getTableNames().split(",");
		codeBase.createAll(params.getDbOwner(),params.getServiceProjectPath(),params.getViewProjectPath(),params.getJavaPackage(), params.getPathFilter(), params.getIgnoePrefixs(),params.getForceOveride(),params.getPrintTableField(),tableNames);
		List<String> fileList=codeBase.getCreatedFiles();
		String zipFile=basePath+".zip";
		try {
			ZipUtils.toZip(basePath,zipFile,true);
		} catch (IOException e) {
			throw e;
		}
		String fileName=params.getTableNames();
		fileName=fileName.replaceAll(",","_")+"-"+baseId+".zip";
		return download(request,response,zipFile,fileName,false);
 	}

	public ResponseEntity download(HttpServletRequest request, HttpServletResponse response, String fullPath,String fileName,Boolean preview) {

		//下载文件路径
		String servletPath=request.getServletPath();
 		if(StringUtils.isEmpty(fileName)){
			fileName=servletPath.substring(servletPath.lastIndexOf("/")+1);

		}
		Resource resource=resourceLoader.getResource("file:"+fullPath);
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException e) {
			logger.error("无法获取文件类型", e);
		}
		if (contentType == null) {
			contentType = "application/octet-stream";
		}
		if( preview){
			return ResponseEntity.ok()
					.contentType(MediaType.parseMediaType(contentType))
					.body(resource);
		}else {
			return ResponseEntity.ok()
					.contentType(MediaType.parseMediaType(contentType))
					.header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + fileName + "\"")
					.body(resource);
		}

	}

}
