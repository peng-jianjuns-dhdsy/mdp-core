package  ${largeModulePackage}.entity;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.mdp.core.dao.annotation.TableIds;
import com.baomidou.mybatisplus.annotation.TableName;
import org.apache.ibatis.type.Alias;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
<#list importClassList as column>
import ${column.fullJavaClassName};
</#list>

/**
 * @author code-gen
 * @since ${.now?date}
 */
@Data
@TableName("${tableName}")
@ApiModel(description="${tableRemarks}")
public class ${entityName}  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	<#if (primaryKeysList?size <= 1)>
	<#list primaryKeysList as column>
	@TableId(type = IdType.ASSIGN_ID)
	<#if (printTableField==true)>@TableField("${column.columnName}")</#if>
	@ApiModelProperty(notes="${column.remarks},主键",allowEmptyValue=true,example="",allowableValues="")
	${column.simpleJavaClassName} ${column.camelsColumnName};
  	</#list>
  	</#if>
    <#if (primaryKeysList?size > 1 )>
    <#list primaryKeysList as column>
    @TableIds
	<#if (printTableField==true)>@TableField("${column.columnName}")</#if>
    @ApiModelProperty(notes="${column.remarks},主键",allowEmptyValue=true,example="",allowableValues="")
    ${column.simpleJavaClassName} ${column.camelsColumnName};
    </#list>
    </#if>
	<#list columnExcludePkList as column>

	<#if (printTableField==true)>@TableField("${column.columnName}")</#if>
	@ApiModelProperty(notes="${column.remarks}",allowEmptyValue=true,example="",allowableValues="")
	${column.simpleJavaClassName} ${column.camelsColumnName};
  	</#list>

	/**
	 *<#list primaryKeysList as column>${column.remarks}<#if column_has_next>,</#if></#list>
	 **/
	public ${entityName}(<#list primaryKeysList as column>${column.simpleJavaClassName} ${column.camelsColumnName}<#if column_has_next>,</#if></#list>) {
		<#list primaryKeysList as column>
		this.${column.camelsColumnName} = ${column.camelsColumnName};
		</#list>
	}
    
    /**
     * ${tableRemarks}
     **/
	public ${entityName}() {
	}

}