package com.mdp.core.dao;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.core.metadata.TableFieldInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.baomidou.mybatisplus.core.toolkit.ClassUtils;
import com.baomidou.mybatisplus.core.toolkit.ReflectionKit;
import com.mdp.core.dao.annotation.TableIds;
import org.apache.ibatis.type.Alias;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TableUtil {



    static Map<Class,List<TableFieldInfo>> TABLE_ALL_FIELDS_CACHE =new HashMap<>();
    static Map<Class,List<TableFieldInfo>> TABLE_PK_FIELDS_CACHE =new HashMap<>();

    static Map<Class,String> TABLE_NAME_ALIAS_CACHE =new HashMap<>();
    static Map<Class,String> TABLE_NAME_CACHE =new HashMap<>();
 

    public static List<TableFieldInfo> getPkFields(Class<?> clazz) {
        if (clazz != null && !ReflectionKit.isPrimitiveOrWrapper(clazz) && clazz != String.class && !clazz.isInterface()) {
            Class<?> targetClass = ClassUtils.getUserClass(clazz);
            return TABLE_PK_FIELDS_CACHE.get(targetClass);
        }else {
            return new ArrayList<>();
        }
    }
    public static List<TableFieldInfo> getAllFields(Class clazz){
        if (clazz != null && !ReflectionKit.isPrimitiveOrWrapper(clazz) && clazz != String.class && !clazz.isInterface()) {
            Class<?> targetClass = ClassUtils.getUserClass(clazz);
            return TABLE_ALL_FIELDS_CACHE.get(targetClass);
        } else {
            return new ArrayList<>();
        }
    }
    public static void setAllFields(Class<?> clazz, List<TableFieldInfo> fieldListIncludeIds) {
        TABLE_ALL_FIELDS_CACHE.put(clazz,fieldListIncludeIds);
        Alias alias=clazz.getAnnotation(Alias.class);
        if(alias!=null && alias.value()!=null && !alias.value().trim().equals("")){
            TABLE_NAME_ALIAS_CACHE.put(clazz,alias.value());
        }
        List<TableFieldInfo> pks=new ArrayList<>();
        List<TableFieldInfo> ids0=new ArrayList<>();
        List<TableFieldInfo> ids1=new ArrayList<>(); 
        List<TableFieldInfo> ids2=new ArrayList<>();
        for (TableFieldInfo fieldInfo : fieldListIncludeIds) {
            if(fieldInfo.getField().getAnnotation(TableIds.class)!=null){
                ids0.add(fieldInfo); 
            }
            if(fieldInfo.getField().getAnnotation(TableId.class)!=null){
                ids1.add(fieldInfo);
            }
            if(fieldInfo.getColumn().equalsIgnoreCase("id")){
                ids2.add(fieldInfo);
            }
        }
        if(ids0.size()>0){
            TABLE_PK_FIELDS_CACHE.put(clazz,ids0);
        }else if(ids1.size()>0){
            TABLE_PK_FIELDS_CACHE.put(clazz,ids1);
        }else if(ids2.size()>0){
            TABLE_PK_FIELDS_CACHE.put(clazz,ids2);
        }else {
            TABLE_PK_FIELDS_CACHE.put(clazz,pks);
        }
    }

    public static String getTableAlias(Class<?> clazz){
        return TABLE_NAME_ALIAS_CACHE.get(clazz);
    }


    public static String getTableName(Class<?> clazz){
        TableInfo tableInfo=TableInfoHelper.getTableInfo(clazz);
        if(tableInfo==null){
            return null;
        }
        return tableInfo.getTableName();
    }
}
