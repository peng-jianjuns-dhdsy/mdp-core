package com.mdp.core.service;

import com.mdp.core.api.Cache;

import java.util.HashMap;
import java.util.Map;

public class DefaultCacheService<T> implements Cache<T> {
	
	Map<String,Object> cache=new HashMap<>();
	
	@Override
	public   void put(String key, T o) {
		cache.put(key, o);
	}

	@Override
	public   T get(String key) {
		 
		return (T) cache.get(key);
	}

	@Override
	public void remove(String key) {
		cache.remove(key);
		
	}

	@Override
	public boolean containsKey(String key) {
		
		return cache.containsKey(key);
	}

	@Override
	public void refresh() {
		cache.clear();
		
	}

	@Override
	public boolean containsValue(T value) {
		
		return cache.containsValue(value);
	}

	@Override
	public boolean expire(long milliseconds) {
		
		return false;
	}

}
