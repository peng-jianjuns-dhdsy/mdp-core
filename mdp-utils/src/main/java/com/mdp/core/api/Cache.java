package com.mdp.core.api;

public interface Cache<T> {

	
	public     void put(String key,T value);
	 
	public   T get(String key);
	
	public void remove(String key);

	boolean containsKey(String key);
	
	void refresh();
	
	boolean containsValue(T value);
	
	boolean expire(long milliseconds);
}
